﻿using JocDame.Models;
using System;
using System.IO;
using System.Collections.ObjectModel;


namespace JocDame.ViewModel
{
   public class AboutVM
    {
        public ObservableCollection<AboutModel> TextVM { set; get; }
        private String text;
        public AboutVM()
        {
            ReadMessage();
            TextVM = new ObservableCollection<AboutModel>()
            {
                new AboutModel{Text=text}
            };


        }

        public void ReadMessage()
        {
            try
            {
                StreamReader streamReader = new StreamReader(@"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\About.txt");
                text = streamReader.ReadToEnd();
                streamReader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }
    }

}
