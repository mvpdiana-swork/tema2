﻿using JocDame.Services;
using System.Windows.Input;
using JocDame.Models;
using JocDame.Commands;

namespace JocDame.ViewModel
{
  public  class CellVM : BaseNotification
    {
        GameBusinessLogic businessLogic;

        public CellVM(int coordinateX, int coordinateY, string pieceType, string background, ref GameBusinessLogic businessLogic)
        {
            SimpleCell = new Cell(coordinateX, coordinateY, pieceType, background);
            this.businessLogic = businessLogic;
        }

        public CellVM(ref Cell cell, ref GameBusinessLogic bl)
        {
            simpleCell = cell;
            this.businessLogic = bl;
        }

        private Cell simpleCell;
        public Cell SimpleCell
        {
            get { return simpleCell; }
            set
            {
                simpleCell = value;
                NotifyPropertyChanged("SimpleCell");
            }
        }

        private ICommand clickCommand;
        public ICommand ClickCommand
        {
            get
            {
                if (clickCommand == null)
                {
                    clickCommand = new RelayCommand<Cell>(businessLogic.ClickAction);
                }
                return clickCommand;
            }
        }
    }
}
