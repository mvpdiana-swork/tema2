﻿using Views.JocDame.Models;
using System.Collections.ObjectModel;
using System;
using System.IO;

namespace JocDame.ViewModel
{
   public class StatisticsViewModel
    {
        public ObservableCollection<StatisticsModel> Statistics { set; get; }
        public String titleTotalGames;
        public String titleRedWons;
        public String titleWhiteWons;
        public String totalGames;
        public String redWons;
        public String whiteWons;
        public StatisticsViewModel()
        {
            ReadStatistics();
            Statistics = new ObservableCollection<StatisticsModel>
            {
              new StatisticsModel{TitleTotalGames=titleTotalGames, TitleRedWon=titleRedWons, TitleWhiteWon=titleWhiteWons,
                TotalGames=totalGames, RedWon=redWons, WhiteWon=whiteWons }
            };

        }
       
        public  void ReadStatistics()
        {
            try
            {
                StreamReader sr = new StreamReader("D:\\Info2\\MVP\\Teme\\JocDame\\Statistics.txt");

               String line = sr.ReadLine();
                    titleTotalGames = line;
                    line = sr.ReadLine();
                    titleRedWons = line;
                    line = sr.ReadLine();
                    titleWhiteWons = line;
                   line = sr.ReadLine();
                    totalGames = line;
                    line = sr.ReadLine();
                    redWons= line;
                    line = sr.ReadLine();
                    whiteWons = line;
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

    }
}
