﻿using JocDame.Models;
using JocDame.Services;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace JocDame.ViewModel
{
   public class GameBoard: BaseNotification
    {
        private ObservableCollection<ObservableCollection<CellVM>> gameBoard;
        private ObservableCollection<ObservableCollection<CellVM>> gameBoard1;
        private GameBusinessLogic businessLogic;
        private GameBusinessLogic businessLogic1;
        public ObservableCollection<ObservableCollection<CellVM>> Board 
        {
            get { return gameBoard; }
            set
            {
                gameBoard = value;
                NotifyPropertyChanged("GameBoard");
            }

        }
        public ObservableCollection<ObservableCollection<CellVM>> Board1
        {
            get { return gameBoard1; }
            set
            {
                gameBoard1 = value;
                NotifyPropertyChanged("GameBoard");
            }

        }
        
        public GameBoard()
        {
            var mySerializer = new XmlSerializer(typeof(GameBusinessLogic));
            var myFileStream = new FileStream(@"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\savedGame.xml", FileMode.Open);
            var myObject = (GameBusinessLogic)mySerializer.Deserialize(myFileStream);
            ObservableCollection<ObservableCollection<Cell>> board = Helper.InitGameBoard();
            ObservableCollection<ObservableCollection<Cell>> board1 = Helper.InitGameBoard();
            businessLogic = new GameBusinessLogic(board);
            businessLogic1 = myObject;
            Board = InitializeBoard(board);
            Board1 = InitializeBoard1(board1);
            myFileStream.Close();
        }
        
        public ObservableCollection<ObservableCollection<CellVM>> InitializeBoard(ObservableCollection<ObservableCollection<Cell>> board)
        {
            ObservableCollection<ObservableCollection<CellVM>> result = new ObservableCollection<ObservableCollection<CellVM>>();
            for (int i = 0; i < board.Count; i++)
            {
                ObservableCollection<CellVM> line = new ObservableCollection<CellVM>();
                for (int j = 0; j < board[i].Count; j++)
                {
                    Cell c = board[i][j];
                    line.Add(new CellVM(ref c, ref businessLogic));
                }
                result.Add(line);
            }
            return result;

        }

        public ObservableCollection<ObservableCollection<CellVM>> InitializeBoard1(ObservableCollection<ObservableCollection<Cell>> board)
        {
            ObservableCollection<ObservableCollection<CellVM>> result = new ObservableCollection<ObservableCollection<CellVM>>();
            for (int i = 0; i < board.Count; i++)
            {
                ObservableCollection<CellVM> line = new ObservableCollection<CellVM>();
                for (int j = 0; j < board[i].Count; j++)
                {
                    Cell c = board[i][j];
                    line.Add(new CellVM(ref c, ref businessLogic1));
                }
                result.Add(line);
            }
            return result;

        }


    }
}
