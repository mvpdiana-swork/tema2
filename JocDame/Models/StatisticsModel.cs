﻿using System;
using System.ComponentModel;


namespace Views.JocDame.Models
{
    
    public class StatisticsModel : INotifyPropertyChanged
    {
        private String titleTotalGames;
        private String titleRedWon;
        private String titleWhiteWon;
        private String totalGames;
        private String redWon;
        private String whiteWon;

        public String TitleTotalGames
        {
            get
            {
                return titleTotalGames;
            }
            set
            {
                titleTotalGames = value;
                OnPropertyChanged("TitleTotalGames");
            }
        }

        public String TitleRedWon
        { 
            get
            {
                return titleRedWon;
            }

            set
            {
                titleRedWon = value;
                OnPropertyChanged("TitleRedWon");
            }
        }

        public String TitleWhiteWon
        {
            get
            {
                return titleWhiteWon;
            }

            set
            {
                titleWhiteWon = value;
                OnPropertyChanged("TitleWhiteWon");
            }
        }

        public String TotalGames
        {
            get
            {
                return totalGames;
            }
            set
            {
                totalGames = value;
                OnPropertyChanged("TotalGames");
            }
        }

        public String RedWon
        {
            get
            {
                return redWon;
            }
            set
            {
                redWon = value;
                OnPropertyChanged("RedWon");
            }
        }

        public String WhiteWon
        {
            get
            {
                return whiteWon;
            }

            set
            {
                whiteWon = value;
                OnPropertyChanged("WhiteWon");
            }
        }

        public override string ToString()
        {
            return $"{TitleTotalGames}: {TotalGames}\n {TitleRedWon}: {RedWon}\n {TitleWhiteWon}: {WhiteWon}";
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }

}
