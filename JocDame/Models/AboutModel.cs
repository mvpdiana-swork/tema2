﻿using System;
using System.ComponentModel;
namespace JocDame.Models
{
  public  class AboutModel: INotifyPropertyChanged
    {
        private String text;
        public String Text
        {
            set
            {
                text = value;
                NotifyPropertyChanged("Text");
            }
            get
            {
                return text;
            }
        }

        public override string ToString()
        {
            return text.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
