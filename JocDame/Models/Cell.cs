﻿using System.ComponentModel;

namespace JocDame.Models
{
  public  class Cell : INotifyPropertyChanged
    {
        private int coordinateX;
        private int coordinateY;
        private string background;
        private string pieceType;

        public Cell()
        {

        }
        public Cell(string background)
        {
            this.background = background;
        }

        public Cell(int CoordinateX, int CoordinateY, string PieceType, string Background = "")
        {
            this.CoordinateX = CoordinateX;
            this.CoordinateY = CoordinateY;
            this.Background = Background;
            this.PieceType = PieceType;
        }
        public int CoordinateX
        {
            get
            {
                return coordinateX;
            }
            set
            {
                coordinateX = value;
                NotifyPropertyChanged("CoordinateX");
            }
        }

        public int CoordinateY
        {
            get
            {
                return coordinateY;
            }
            set
            {
                coordinateY = value;
                NotifyPropertyChanged("CoordinateY");
            }
        }

        public string Background
        {
            get
            {
                return background;
            }
            set
            {
                background = value;
                NotifyPropertyChanged("Background");
            }
        }

      
        public string PieceType
        {
            get
            {
                return pieceType;
            }
            set
            {
                pieceType = value;
                NotifyPropertyChanged("PieceType");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
