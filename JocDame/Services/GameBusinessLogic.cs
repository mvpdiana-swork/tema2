﻿using JocDame.Models;
using JocDame.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace JocDame.Services
{
    public class GameBusinessLogic
    {
        public Cell currentCell;
        public Cell previousCell;
        public int whitePieces = 12;
        public int redPieces = 12;
        public bool firstClick = true;
        public bool firstPlayerTurn = true;
        public bool secondPlayerTurn = false;
        public static GameBusinessLogic gameBusinessLogic=new GameBusinessLogic();
        public ObservableCollection<ObservableCollection<Cell>> board;
        public GameBusinessLogic()
        {

        }
        public GameBusinessLogic(ObservableCollection<ObservableCollection<Cell>> board)
        {
            this.board = board;
        }
        public static void SaveGame()
        {
           
            XmlSerializer mySerializer = new XmlSerializer(typeof(GameBusinessLogic));
            StreamWriter myWriter = new StreamWriter(@"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\savedGame.xml");
            mySerializer.Serialize(myWriter, gameBusinessLogic); 
            myWriter.Close();
        }
        public void FirstClickAction()
        {
            if ((currentCell.PieceType == "red" || currentCell.PieceType == "redKing") && firstPlayerTurn == true)
            {
                previousCell = currentCell;
                firstClick = false;
                firstPlayerTurn = false;
                secondPlayerTurn = true;

            }
            if ((currentCell.PieceType == "white" || currentCell.PieceType == "whiteKing") && secondPlayerTurn == true)
            {
                previousCell = currentCell;
                firstClick = false;
                firstPlayerTurn = true;
                secondPlayerTurn = false;
            }
        }
        public void Swap()
        {
            Cell copy = new Cell();
            copy.Background = currentCell.Background;
            copy.PieceType = currentCell.PieceType;
            board[currentCell.CoordinateX][currentCell.CoordinateY].Background = previousCell.Background;
            board[currentCell.CoordinateX][currentCell.CoordinateY].PieceType = previousCell.PieceType;
            board[previousCell.CoordinateX][previousCell.CoordinateY].Background = copy.Background;
            board[previousCell.CoordinateX][previousCell.CoordinateY].PieceType = copy.PieceType;

        }
        public bool RedKingMove()
        {
            if (currentCell.CoordinateX + 2 != previousCell.CoordinateX && currentCell.CoordinateX - 2 != previousCell.CoordinateX)
            {
                return false;
            }
            if (currentCell.CoordinateY + 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "white" ||
                 board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "whiteKing"))
            {
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType = "none";
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].Background =
                   @"..\Resources\red.jpg";
                whitePieces--;
                return true;
            }
            if (currentCell.CoordinateY + 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "white" ||
                  board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "whiteKing"))
            {
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType = "none";
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].Background =
                   @"..\Resources\red.jpg";
                whitePieces--;
                return true;
            }

            if (currentCell.CoordinateY - 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "white" ||
                  board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "whiteKing"))
            {
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType = "none";
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].Background =
                    @"..\Resources\red.jpg";
                whitePieces--;
                return true;
            }

            if (currentCell.CoordinateY - 2 == previousCell.CoordinateY &&
                (board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "white" ||
                 board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "whiteKing"))
            {
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType = "none";
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].Background =
                    @"..\Resources\red.jpg";
                whitePieces--;
                return true;
            }
            return false;
        }

        public bool WhiteKingMove()
        {
            if (currentCell.CoordinateX + 2 != previousCell.CoordinateX && currentCell.CoordinateX - 2 != previousCell.CoordinateX)
            {
                return false;
            }
            if (currentCell.CoordinateY + 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "red" ||
                 board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "redKing"))
            {
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType = "none";
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].Background =
                   @"..\Resources\red.jpg";
                redPieces--;
                return true;
            }
            if (currentCell.CoordinateY + 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "red" ||
                  board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "redKing"))
            {
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType = "none";
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].Background =
                   @"..\Resources\red.jpg";
                redPieces--;
                return true;
            }

            if (currentCell.CoordinateY - 2 == previousCell.CoordinateY &&
                 (board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "red" ||
                  board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "redKing"))
            {
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType = "none";
                board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].Background =
                    @"..\Resources\red.jpg";
                redPieces--;
                return true;
            }

            if (currentCell.CoordinateY - 2 == previousCell.CoordinateY &&
                (board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "red" ||
                 board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "redKing"))
            {
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType = "none";
                board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].Background =
                    @"..\Resources\red.jpg";
                redPieces--;
                return true;
            }
            return false;
        }
        public bool CaputreMove()
        {
            if (currentCell.PieceType != "none")
                return false;

            if (previousCell.PieceType == "red")
            {
                if (currentCell.CoordinateX + 2 != previousCell.CoordinateX)
                {
                    return false;
                }
                if (currentCell.CoordinateY - 2 == previousCell.CoordinateY &&
                    (board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "white" ||
                     board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType == "whiteKing"))
                {
                    board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].PieceType = "none";
                    board[previousCell.CoordinateX - 1][previousCell.CoordinateY + 1].Background =
                       @"..\Resources\red.jpg";
                    whitePieces--;
                    return true;
                }

                if (currentCell.CoordinateY + 2 == previousCell.CoordinateY &&
                     (board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "white" ||
                     board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType == "white"))
                {
                    board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].PieceType = "none";
                    board[previousCell.CoordinateX - 1][previousCell.CoordinateY - 1].Background =
                        @"..\Resources\red.jpg";
                    whitePieces--;
                    return true;
                }
            }
            if (previousCell.PieceType == "redKing")
            {
                return RedKingMove();
            }
            if (previousCell.PieceType == "white")
            {
                if (currentCell.CoordinateX - 2 != previousCell.CoordinateX)
                {
                    return false;
                }
                if (currentCell.CoordinateY + 2 == previousCell.CoordinateY
                    && (board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "red" ||
                    board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType == "redKing"))
                {
                    board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].PieceType = "none";
                    board[previousCell.CoordinateX + 1][previousCell.CoordinateY - 1].Background =
                        @"..\Resources\red.jpg";
                    redPieces--;
                    return true;
                }
                if (currentCell.CoordinateY - 2 == previousCell.CoordinateY
                    && (board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "red" ||
                     board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType == "redKing"))
                {
                    board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].PieceType = "none";
                    board[previousCell.CoordinateX + 1][previousCell.CoordinateY + 1].Background =
                        @"..\Resources\red.jpg";
                    redPieces--;
                    return true;
                }
            }

            if (previousCell.PieceType == "whiteKing")
            {
                return WhiteKingMove();
            }

            return false;
        }
        public bool IsPossibleMove()
        {
            if (currentCell.PieceType != "none")
                return false;

            if (previousCell.PieceType == "red")
            {
                if (currentCell.CoordinateX + 1 != previousCell.CoordinateX)
                {
                    return false;
                }
                if (currentCell.CoordinateY - 1 != previousCell.CoordinateY && currentCell.CoordinateY + 1
                    != previousCell.CoordinateY)
                {
                    return false;
                }
            }

            if (previousCell.PieceType == "white")
            {
                if (currentCell.CoordinateX - 1 != previousCell.CoordinateX)
                {
                    return false;
                }
                if (currentCell.CoordinateY - 1 != previousCell.CoordinateY && currentCell.CoordinateY + 1
                    != previousCell.CoordinateY)
                {
                    return false;
                }
            }
            if (previousCell.PieceType == "redKing" || previousCell.PieceType == "whiteKing")
            {
                if (currentCell.CoordinateX - 1 != previousCell.CoordinateX &&
                    currentCell.CoordinateX + 1 != previousCell.CoordinateX)
                {
                    return false;
                }
                if (currentCell.CoordinateY - 1 != previousCell.CoordinateY &&
                    currentCell.CoordinateY + 1 != previousCell.CoordinateY)
                {
                    return false;
                }
            }
            return true;

        }

        public bool IsGameOver()
        {
            gameBusinessLogic = this;
            if (redPieces == 0)
            {
                MessageBox.Show("WINNER: WHITE!");
                StatisticsViewModel statisticsViewModel = new StatisticsViewModel();
                int numberOfGames = Int32.Parse(statisticsViewModel.totalGames);
                numberOfGames++;
                string totalGames = numberOfGames.ToString();
                int whiteWins = Int32.Parse(statisticsViewModel.whiteWons);
                whiteWins++;
                string totalWhite = whiteWins.ToString();
                File.Delete(@"D:\Info2\MVP\Teme\JocDame\Statistics.txt");
                using (StreamWriter sw = File.AppendText(@"D:\Info2\MVP\Teme\JocDame\Statistics.txt"))
                {
                    sw.WriteLine(statisticsViewModel.titleTotalGames);
                    sw.WriteLine(statisticsViewModel.titleRedWons);
                    sw.WriteLine(statisticsViewModel.titleWhiteWons);
                    sw.WriteLine(totalGames);
                    sw.WriteLine(statisticsViewModel.redWons);
                    sw.WriteLine(totalWhite);
                    sw.Close();
                }
                return true;
            }
            if (whitePieces == 0)
            {
                System.Windows.MessageBox.Show("WINNER: RED!");
                StatisticsViewModel statisticsViewModel = new StatisticsViewModel();
                int numberOfGames = Int32.Parse(statisticsViewModel.totalGames);
                numberOfGames++;
                string totalGames = numberOfGames.ToString();
                int redWins = Int32.Parse(statisticsViewModel.redWons);
                redWins++;
                string totalRed = redWins.ToString();
                File.Delete(@"D:\Info2\MVP\Teme\JocDame\Statistics.txt");// Deleting the file
                using (StreamWriter sw = File.AppendText(@"D:\Info2\MVP\Teme\JocDame\Statistics.txt"))
                {
                    sw.WriteLine(statisticsViewModel.titleTotalGames);
                    sw.WriteLine(statisticsViewModel.titleRedWons);
                    sw.WriteLine(statisticsViewModel.titleWhiteWons);
                    sw.WriteLine(totalGames);
                    sw.WriteLine(totalRed);
                    sw.WriteLine(statisticsViewModel.whiteWons);
                    sw.Close();
                }
                return true;
            }
            return false;
        }
        public void King()
        {
            if (currentCell.PieceType == "red")
            {
                if (currentCell.CoordinateX == 0)
                {
                    currentCell.Background = @"..\Resources\redKing.png";
                    currentCell.PieceType = "redKing";
                }
            }

            if (currentCell.PieceType == "white")
            {
                if (currentCell.CoordinateX == 7)
                {
                    currentCell.Background = @"..\Resources\whiteKing.png";
                    currentCell.PieceType = "whiteKing";
                }
            }
        }

        public void ClickAction(Cell obj)
        {
            currentCell = obj;
            if (firstClick == true)
            {
                if (firstPlayerTurn == true && (currentCell.PieceType == "white" || currentCell.PieceType == "whiteKing"))
                {
                    MessageBox.Show("Red turn!");
                }
                if (secondPlayerTurn == true && (currentCell.PieceType == "red" || currentCell.PieceType == "redKing"))
                {
                    MessageBox.Show("White turn!");
                }
                FirstClickAction();
            }
            else
            {
                if (IsPossibleMove() == true)
                {
                    Swap();
                    firstClick = true;
                }
                else
                {
                    if (CaputreMove() == true)
                    {
                        Swap();
                        if (IsGameOver() == true)
                        {
                            //toDo

                        }
                        firstClick = true;
                    }
                    else
                    {
                        firstClick = true;
                        if (firstPlayerTurn == true)
                        {
                            firstPlayerTurn = false;
                            secondPlayerTurn = true;
                        }
                        else
                        {
                            firstPlayerTurn = true;
                            secondPlayerTurn = false;
                        }
                    }

                }
                King();
            }
        }
    }
}
