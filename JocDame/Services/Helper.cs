﻿using JocDame.Models;
using System.Collections.ObjectModel;

namespace JocDame.Services
{
    class Helper
    {
       
        public static ObservableCollection<ObservableCollection<Cell>> InitGameBoard()
        {
            ObservableCollection<ObservableCollection<Cell>> board = new ObservableCollection<ObservableCollection<Cell>>();

            for (int i = 0; i < 8; i++)
            {
                ObservableCollection<Cell> line = new ObservableCollection<Cell>();
                for (int j = 0; j < 8; j++)
                {
                    if (i < 3)
                    {
                        if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\white.jpg",
                                PieceType="none"
                            };
                            line.Add(cell);
                        }
                        else if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\whitePiece.png",
                                PieceType = "white"
                            };
                            line.Add(cell);
                        }
                    }
                    else if(i<5)
                    {
                        if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\white.jpg",
                                PieceType = "none"
                            };
                            line.Add(cell);
                        }
                        else if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\red.jpg",
                                PieceType = "none"
                            };
                            line.Add(cell);
                        }
                    }
                    else
                    {
                        if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\white.jpg",
                                PieceType = "none"
                            };
                            line.Add(cell);
                        }
                        else if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0))
                        {
                            Cell cell = new Cell
                            {
                                CoordinateX = i,
                                CoordinateY = j,
                                Background = @"D:\Info2\MVP\Teme\JocDame\JocDame\Resources\redPiece.png",
                                PieceType = "red"
                            };
                            line.Add(cell);
                        }
                    }


                }
                board.Add(line);
            }

            return board;
        }
    }
}

