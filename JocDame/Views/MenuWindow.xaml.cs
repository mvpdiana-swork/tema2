﻿using JocDame.Services;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace JocDame.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            InitializeComponent();
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            StatisticsView statisticsView = new StatisticsView();
            statisticsView.Show();

        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            Game game = new Game();
            game.Show();
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            About about = new About();
            about.Show();
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            LoadGame load = new LoadGame();
            load.Show();
            
        }
    }
}
