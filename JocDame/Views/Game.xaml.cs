﻿using JocDame.Services;
using JocDame.Views;
using System.Windows;


namespace JocDame
{
    /// <summary>
    /// Interaction logic for NewGame.xaml
    /// </summary>
    public partial class Game : Window
    {
        public Game()
        {
            InitializeComponent();
            Closing += new System.ComponentModel.CancelEventHandler(Game_Closing);
        }

        
        void Game_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("DO YOU WANT TO SAVE THIS GAME?", "CLOSING", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                GameBusinessLogic.SaveGame();
            }
           
                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();
            
        }

    }
}
