﻿using JocDame.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JocDame.Views
{
    /// <summary>
    /// Interaction logic for LoadGame.xaml
    /// </summary>
    public partial class LoadGame : Window
    {
        public LoadGame()
        {
            InitializeComponent();
            Closing += new System.ComponentModel.CancelEventHandler(LoadGame_Closing);
        }
        void LoadGame_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("DO YOU WANT TO SAVE THIS GAME?", "CLOSING", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                GameBusinessLogic.SaveGame();
            }

            MenuWindow menuWindow = new MenuWindow();
            menuWindow.Show();

        }
    }
}
